﻿using ictech.Entities.Sql.Ictech;

namespace ictech.Repository.Interfaces.Sql.Ictech
{
    public interface IUsersRepository
    {
        Task<IList<UsersEntity>> GetAsync();
    }
}
