﻿using ictech.Business.Interfaces;
using ictech.Configurations.JwtConfigs;
using ictech.Dto.Users.Response;
using ictech.Repository.Interfaces.Sql.Ictech;

namespace ictech.Business
{
    public class UsersServices : IUsersServices
    {
        private readonly IUsersRepository usersRepository;
        private readonly IJwtConfigs jwtConfigs;
        public UsersServices(IUsersRepository usersRepository,
           IJwtConfigs jwtConfigs)
        {
            this.usersRepository = usersRepository;
            this.jwtConfigs = jwtConfigs;
        }
        public async Task<UsersResponse> Get()
        {
            UsersResponse response = new();
            response.data = await usersRepository.GetAsync();
            return response;
        }


    }
}