﻿namespace ictech.Configuration.ConnectionStrings
{
    public interface IConnectionStrings
    {
        string IctechConnection { get; set; }
    }
}
