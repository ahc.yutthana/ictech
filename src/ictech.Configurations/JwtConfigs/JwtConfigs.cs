﻿namespace ictech.Configurations.JwtConfigs
{
    public class JwtConfigs : IJwtConfigs
    {
        public string Secret { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public int Expires { get; set; }
    }
}
