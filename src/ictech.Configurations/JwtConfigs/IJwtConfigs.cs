﻿namespace ictech.Configurations.JwtConfigs
{
    public interface IJwtConfigs
    {
        string Secret { get; set; }
        string Issuer { get; set; }
        string Audience { get; set; }
        int Expires { get; set; }
    }
}
