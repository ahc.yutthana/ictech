﻿using ictech.Dto.Users.Response;

namespace ictech.Business.Interfaces
{
    public interface IUsersServices
    {
        Task<UsersResponse> Get();
    }
}