﻿using ictech.Configuration.ConnectionStrings;
using ictech.Entities.Sql.Ictech;
using ictech.Repository.Interfaces.Sql.Ictech;
using MySql.Data.MySqlClient;
using System.Data;
using System.Text;

namespace ictech.Repository.Sql.Ictech
{
    public class UsersRepository : IUsersRepository
    {
        private readonly string IctechConnection;

        public UsersRepository(IConnectionStrings connectionStrings)
        {
            IctechConnection = Encoding.UTF8.GetString(Convert.FromBase64String(connectionStrings.IctechConnection));
        }
        public async Task<IList<UsersEntity>> GetAsync()
        {
            List<UsersEntity> responses = new List<UsersEntity>();
            using (MySqlConnection connection = new MySqlConnection(IctechConnection))
            {
                connection.Open();
                try
                {
                    string Sql = "PD001_GET_USERS";
                    using (MySqlCommand cmd = new MySqlCommand(Sql, connection))
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 3600;
                        using (var reader = await cmd.ExecuteReaderAsync())
                        {
                            while (reader.Read())
                            {
                                responses.Add(new UsersEntity
                                {
                                    id = int.TryParse(reader["id"].ToString(), out int id) ? id : 0,
                                    fname = reader["fname"].ToString(),
                                    lname = reader["lname"].ToString(),
                                    department_id = int.TryParse(reader["department_id"].ToString(), out int department_id) ? department_id : 0,
                                    department = reader["department"].ToString(),
                                    level = int.TryParse(reader["level"].ToString(), out int level) ? level : 0,
                                });
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
            return responses;
        }

    }
}
