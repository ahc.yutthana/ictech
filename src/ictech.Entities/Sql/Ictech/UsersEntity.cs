﻿namespace ictech.Entities.Sql.Ictech
{
    public class UsersEntity
    {
        public int id { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public int department_id { get; set; }
        public string department { get; set; }
        public string division { get; set; }
        public string position { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public string address { get; set; }
        public string line { get; set; }
        public string remark { get; set; }
        public string img { get; set; }
        public int level { get; set; }
        public int online_status { get; set; }
        public int status { get; set; }
        public DateTime create_date { get; set; }
        public int create_by { get; set; }
        public DateTime update_date { get; set; }
        public int update_by { get; set; }

    }
}
