﻿using ictech.Dto;
using ictech.GlobalExeptions;
using Newtonsoft.Json;
using System.Net;

namespace ictech.WebApi.Nlog
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context /* other dependencies */)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            MasterResponse response = new();
            response.status = false;
            response.message = exception.Message;
            response.message_th = exception.Message;
            int code;

            switch (exception)
            {
                case AccountNotfoundException:
                    code = (int)HttpStatusCode.BadRequest;
                    response.message = $"Not found account: {exception.Message}!";
                    response.message_th = $"ไม่พบบัญชี: {exception.Message}!";
                    break;

                case DataNotfoundException:
                    code = (int)HttpStatusCode.BadRequest;
                    break;

                case InvalidPasswordException:
                    code = (int)HttpStatusCode.BadRequest;
                    response.message = "Invalid your password!";
                    response.message_th = $"รหัสผ่านไม่ถูกต้อง!";
                    break;

                case AccountInactiveException:
                    code = (int)HttpStatusCode.BadRequest;
                    break;
                case DataExistAlreadyException:
                    code = (int)HttpStatusCode.BadRequest;
                    break;

                default:
                    // unhandled error
                    code = (int)HttpStatusCode.InternalServerError;
                    Logger.Instance.Error(exception);

                    if (exception.Message.Contains("duplicate"))
                    {
                        code = (int)HttpStatusCode.BadRequest;
                        response.message = "Username is already exist!";
                        response.message_th = "ชื่อผู้ใช้งานซ้ำ!";
                    }
                    break;
            }
            var result = JsonConvert.SerializeObject(response);
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = code;
            return context.Response.WriteAsync(result);
        }

    }
}
