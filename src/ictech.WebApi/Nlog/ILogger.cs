﻿namespace ictech.WebApi.Nlog
{
    public interface ILogger
    {
        void Info(string message);
        void Debug(string message);
        void Error(string message);
        void Error(Exception ex);
    }
}
