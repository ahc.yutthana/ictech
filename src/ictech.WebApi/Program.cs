using ictech.Business;
using ictech.Business.Interfaces;
using ictech.Configuration.ConnectionStrings;
using ictech.Configurations.JwtConfigs;
using ictech.Repository.Interfaces.Sql.Ictech;
using ictech.Repository.Sql.Ictech;
using ictech.WebApi.Nlog;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

// Add services to the container.
IServiceCollection services = builder.Services;

services.AddControllersWithViews();
services.AddEndpointsApiExplorer();
services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "Ictech api", Version = "v1" });
    var securityScheme = new OpenApiSecurityScheme()
    {
        Description = "Please enter into field the word 'Bearer' followed by a space and the JWT value",
        Name = "Authorization",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.Http,
        BearerFormat = "JWT",
        Scheme = "bearer"
    };
    c.AddSecurityDefinition("Bearer", securityScheme);
    c.AddSecurityRequirement(new OpenApiSecurityRequirement { { new OpenApiSecurityScheme { Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "Bearer" } }, new string[] { } } });
});
services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

// Add service
services.AddScoped<IUsersServices, UsersServices>();

// Add repository
services.AddScoped<IUsersRepository, UsersRepository>();

// Add Helper
//services.AddScoped<IEmailHelper, EmailHelper>();

// Add configuration
ConfigurationManager Configuration = builder.Configuration;
var env = builder.Environment;
Configuration.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);

services.Configure<ConnectionStrings>(Configuration.GetSection(nameof(ConnectionStrings)));
services.AddSingleton<IConnectionStrings>(sp => sp.GetRequiredService<IOptions<ConnectionStrings>>().Value);

services.Configure<JwtConfigs>(Configuration.GetSection(nameof(JwtConfigs)));
services.AddSingleton<IJwtConfigs>(sp => sp.GetRequiredService<IOptions<JwtConfigs>>().Value);

JwtConfigs jwt = Configuration.GetSection("JwtConfigs").Get<JwtConfigs>();
var key = Encoding.ASCII.GetBytes(Encoding.UTF8.GetString(Convert.FromBase64String(jwt.Secret)));
services.AddAuthentication(x =>
{
    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme; // allow roles
})
.AddJwtBearer(x =>
{
    x.RequireHttpsMetadata = false;
    x.SaveToken = true;
    x.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey(key),
        ValidateIssuer = true,
        ValidateAudience = true,
        ValidateLifetime = true,
        ValidIssuer = jwt.Issuer,
        ValidAudience = jwt.Audience,
        ClockSkew = TimeSpan.Zero
    };
});

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseMiddleware<ErrorHandlingMiddleware>();

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseAuthentication();
app.UseRouting();
app.UseAuthorization();


app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");

app.MapFallbackToFile("index.html");

app.Run();
