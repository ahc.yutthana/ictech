﻿using ictech.Business.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace ictech.WebApi.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUsersServices usersServices;
        public UsersController(IUsersServices usersServices)
        {
            this.usersServices = usersServices;
        }
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await usersServices.Get());
        }


        //[HttpGet(Name = "GetUsers")]
        //public async Task<IActionResult> Get()
        //{
        //    return Ok(await usersServices.Get());
        //}


    }
}
