using ictech.Business.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace ictech.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IUsersServices usersServices;
        public WeatherForecastController(ILogger<WeatherForecastController> logger, IUsersServices usersServices)
        {
            _logger = logger;
            this.usersServices = usersServices;
        }

        [HttpGet(Name = "GetWeatherForecast")]
        public async Task<IActionResult> Get()
        {
            return Ok(await usersServices.Get());
        }

        //public IEnumerable<WeatherForecast> Get()
        //{
        //    return Ok(await usersServices.Get());

        //    //var aaa = Ok(usersServices.Get());
        //    //return Enumerable.Range(1, 5).Select(index => new WeatherForecast
        //    //{
        //    //    Date = DateTime.Now.AddDays(index),
        //    //    TemperatureC = Random.Shared.Next(-20, 55),
        //    //    Summary = Summaries[Random.Shared.Next(Summaries.Length)]
        //    //})
        //    //.ToArray();
        //}

        //[HttpGet]
        //public IEnumerable<WeatherForecast> GetUsersx()
        //{
        //    //var aaa = Ok(usersServices.Get());
        //    return Enumerable.Range(1, 5).Select(index => new WeatherForecast
        //    {
        //        Date = DateTime.Now.AddDays(index),
        //        TemperatureC = Random.Shared.Next(-20, 55),
        //        Summary = Summaries[Random.Shared.Next(Summaries.Length)]
        //    })
        //   .ToArray();
        //}


        //[HttpGet]
        //public async Task<IActionResult> Get()
        //{
        //    return Ok(await usersServices.Get());
        //}

    }
}