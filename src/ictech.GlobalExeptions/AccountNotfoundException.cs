﻿namespace ictech.GlobalExeptions
{
    public class AccountNotfoundException : Exception
    {
        public AccountNotfoundException(string message) : base(message) { }
    }
}