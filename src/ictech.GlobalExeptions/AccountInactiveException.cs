﻿namespace ictech.GlobalExeptions
{
    public class AccountInactiveException : Exception
    {
        public AccountInactiveException() : base("Account inactive please contact administrator!") { }
    }
}