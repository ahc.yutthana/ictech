﻿namespace ictech.GlobalExeptions
{
    public class DataNotfoundException : Exception
    {
        public DataNotfoundException() : base("Data not found!") { }
    }
}