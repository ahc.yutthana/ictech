﻿namespace ictech.GlobalExeptions
{
    public class DataExistAlreadyException : Exception
    {
        public DataExistAlreadyException() : base("Data is exist already!") { }
    }
}
