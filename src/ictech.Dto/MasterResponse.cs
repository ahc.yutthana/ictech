﻿namespace ictech.Dto
{
    public class MasterResponse
    {
        public bool status { get; set; } = true;
        public string message { get; set; } = "Success!";
        public string message_th { get; set; } = "ดำเนินการเรียบร้อย!";
    }
}
