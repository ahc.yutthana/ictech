﻿namespace ictech.Dto.Users.Response
{
    public class UsersResponse : MasterResponse
    {
        public IList<Entities.Sql.Ictech.UsersEntity> data { get; set; }
    }
}
